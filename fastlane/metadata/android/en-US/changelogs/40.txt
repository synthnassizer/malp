* Material You design implementation
* Flickering fixes for album images
* ArtistSort fixes
* Albumartist fixes
* MPD Error propagation fixes
* Monochromatic launcher icon
* Smaller fixes
